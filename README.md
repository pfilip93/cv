# CV #

** Hand detection application based on OpenCV **

Written in Java

![screenshot.png](screenshot.png)

----------------------------------------------------------------------

### Requirements ###

* Java SDK
* Apache Maven
* OpenCV Library

### Instalation ###

To develop this project, first you need Java environment with Maven (recomended IDE is IntelliJ).

To use OpenCV, clone this repo: [https://github.com/dermotte/LIRE](https://github.com/dermotte/LIRE)

Next, in Run/Debug Configuration (IntelliJ) add VM options:

`-Djava.library.path="C:\LIRE-master\lib\opencv"`

with path where library is located.